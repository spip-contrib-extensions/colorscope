<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/colorscope.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'colorscope_description' => 'Remplace les codes hexa des couleurs dans un texte SPIP, par un bloc de la dite couleur, en conservant son code à l’intérieur.',
	'colorscope_slogan' => 'Colorer les codes hexa'
);
